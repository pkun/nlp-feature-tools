package net.semanticlab.nlp.ngram;

import java.io.IOException;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.google.common.base.Charsets;
import com.google.common.io.Resources;
import com.weblyzard.api.model.document.Document;

import lombok.extern.slf4j.Slf4j;

/**
 * Helper method for reading json files.
 * 
 * @author Albert Weichselbraun
 *
 */
@Slf4j
public class TestHelper {

    private final static ObjectMapper OBJECT_MAPPER = new ObjectMapper();

    public static Document readJson(String fname, Class<Document> type) {
        try {
            return OBJECT_MAPPER.readValue(Resources.toString(Resources.getResource("Jeremia-61.json"), Charsets.UTF_8), type);
        } catch (IOException e) {
            log.error("Cannot read file '{}': {}", fname, e);
        }
        return null;

    }
}
