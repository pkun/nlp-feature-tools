package net.semanticlab.nlp.ngram.helper.annotation;

import static org.junit.jupiter.api.Assertions.assertFalse;

import org.junit.jupiter.api.Test;

import com.weblyzard.api.model.document.Document;
import com.weblyzard.api.model.document.partition.CharSpan;

class EmptyAnnotationHelperTest {

  @Test
  void testEmptyAnnotationHelper() {
      AnnotationHelperInterface empty = AnnotationHelperInterface.getAnnotationHelper(new Document());
      assertFalse(empty.isInAnnotation(new CharSpan(1,10)));
  }
}
