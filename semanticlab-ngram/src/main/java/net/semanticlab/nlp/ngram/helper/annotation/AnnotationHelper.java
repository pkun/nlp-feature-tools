package net.semanticlab.nlp.ngram.helper.annotation;

import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.function.BiFunction;
import java.util.stream.Collectors;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.annotation.Annotation;
import com.weblyzard.api.model.document.Document;
import net.semanticlab.nlp.ngram.NGram;

/**
 * A {@link AnnotationHelperInterface} implementation that is used with annotations.
 * 
 * @author Albert Weichselbraun
 */
public class AnnotationHelper implements AnnotationHelperInterface {

    private final Document document;
    private final Iterator<Annotation> annotationsIterator;
    private Annotation currentAnnotation;

    public AnnotationHelper(Document document) {
        this.document = document;
        annotationsIterator = document.getAnnotations().stream()
                        .sorted(Comparator.comparing(Annotation::getStart))
                        .collect(Collectors.toList()).iterator();
        currentAnnotation = annotationsIterator.next();
    }

    /**
     * Returns a list of all {@link NGram}s contributed by {@link Annotation}s.
     */
    @Override
    public List<NGram> getAnnotationNgrams(BiFunction<Annotation, Document, NGram> ngramAssembly) {
        return document.getAnnotations().stream()
                        .map(annotation -> ngramAssembly.apply(annotation, document))
                        .collect(Collectors.toList());
    }

    /**
     * Indicates whether the given {@link Span} overlaps with an {@link Annotation}.
     */
    @Override
    public boolean isInAnnotation(Span s) {
        // Check, whether there are still annotations to be considered
        if (currentAnnotation == null) {
            return false;
        }

        // Move forward, if required
        while (currentAnnotation.getEnd() < s.getStart()) {
            if (!annotationsIterator.hasNext()) {
                currentAnnotation = null;
                return false;
            }
            currentAnnotation = annotationsIterator.next();
        }

        return currentAnnotation.getStart() < s.getEnd();
    }


}
