/**
 * Contains classes and interfaces for handling document annotations.
 * 
 * @author Albert Weichselbraun
 *
 */
package net.semanticlab.nlp.ngram.helper.annotation;
