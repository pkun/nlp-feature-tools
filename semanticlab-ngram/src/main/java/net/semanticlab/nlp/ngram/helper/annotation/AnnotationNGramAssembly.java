package net.semanticlab.nlp.ngram.helper.annotation;

import java.util.function.BiFunction;
import com.weblyzard.api.model.annotation.Annotation;
import com.weblyzard.api.model.document.Document;
import net.semanticlab.nlp.ngram.NGram;

/**
 * Provides functions that transform {@link Annotation}s to the corresponding nGrams.
 * 
 * @author Albert Weichselbraun
 *
 */
public class AnnotationNGramAssembly {

    /**
     * Provides an assembly that always returns the surface form of the {@link Annotation}.
     */
    public static BiFunction<Annotation, Document, NGram> getSurfaceFormAssembly() {
        return (annotation,
                        document) -> new NGram(new String[] {document.getContent()
                                        .substring(annotation.getStart(), annotation.getEnd())},
                                        new String[] {"NE"});
    }

    /**
     * Provides an assembly that returns an optional prefix and the key of the {@link Annotation}.
     */
    public static BiFunction<Annotation, Document, NGram> getEntityDescriptorAssembly(
                    String prefix) {
        return (annotation, document) -> new NGram(new String[] {prefix + annotation.getKey()},
                        new String[] {"NE"});
    }

}
