package net.semanticlab.nlp.ngram.transform;

import net.semanticlab.nlp.ngram.NGram;

/**
 * Modifies or filters extracted {@link NGram}s.
 * 
 * @author Albert Weichselbraun
 *
 */
public interface NGramTransformer {

    /**
     * Transforms the given {@link NGram}, if required.
     * 
     * @param ngram the {@link NGram} to transform.
     * @return false, if the ngram should be filtered.
     */
    public boolean transform(NGram ngram);

}
