package net.semanticlab.nlp.ngram;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.function.BiFunction;
import com.weblyzard.api.model.annotation.Annotation;
import com.weblyzard.api.model.document.Document;
import net.semanticlab.nlp.dawg.DawgFactory;
import net.semanticlab.nlp.ngram.helper.GrammarGroupMapper;
import net.semanticlab.nlp.ngram.helper.annotation.AnnotationHelperInterface;
import net.semanticlab.nlp.ngram.helper.sentence.NoFilter;
import net.semanticlab.nlp.ngram.helper.sentence.SentenceFilter;
import net.semanticlab.nlp.ngram.helper.sentence.SentenceFragmentIterator;
import net.semanticlab.nlp.ngram.transform.NGramTransformer;

/**
 * Tokenizes a weblyzard {@link Document} into ngrams based on the provided grammar patterns.
 * 
 * @author Albert Weichselbraun
 *
 */
public class DocumentTokenizer {

    private static final DawgFactory<Integer> dawgFactory = new DawgFactory<>();

    private final TokenExtractor tokenExtractor;
    private final BiFunction<Annotation, Document, NGram> ngramAssembly;
    private final boolean ignoreTitles;

    /**
     * Creates a {@link DocumentTokenizer} based on the given posGrammarGroupMap and
     * validGrammarGroupPatterns.
     * 
     * @param posGrammarGroupMap a mapping between pos tags and grammar groups.
     * @param validGrammarGroupPatterns grammar group patterns to extract.
     */
    public DocumentTokenizer(Map<String, String> posGrammarGroupMap,
                    List<String> validGrammarGroupPatterns, List<NGramTransformer> ngramTransformer,
                    BiFunction<Annotation, Document, NGram> ngramAssembly, boolean ignoreTitles) {

        GrammarGroupMapper grammarMapping = new GrammarGroupMapper(posGrammarGroupMap);
        List<List<Integer>> needles =
                        grammarMapping.getNumericalValidGrammarGroupSpec(validGrammarGroupPatterns);
        tokenExtractor = new TokenExtractor(dawgFactory.getDawg(needles),
                        grammarMapping::getGrammarGroup, dawgFactory.getNeedleRank(needles),
                        ngramTransformer);
        this.ngramAssembly = ngramAssembly;
        this.ignoreTitles = ignoreTitles;
    }

    /**
     * Tokenizes the given {@link Document}.
     * 
     * @param document the document to tokenize.
     * @param sentenceFilter allows filtering of sentences that should not be considered in the
     *        tokenization process.
     * @return the list of extracted ngrams.
     */
    public List<NGram> tokenize(Document document, SentenceFilter sentenceFilter) {
        AnnotationHelperInterface annotationHelper =
                        AnnotationHelperInterface.getAnnotationHelper(document);
        List<NGram> result = new ArrayList<>(annotationHelper.getAnnotationNgrams(ngramAssembly));
        SentenceFragmentIterator tokenSequence;
        try {
            tokenSequence = new SentenceFragmentIterator(document, annotationHelper,
                            sentenceFilter);
        } catch (IllegalArgumentException e) {
            return Collections.emptyList();
        }

        String content = document.getContent();
        while (tokenSequence.hasNext()) {
            result.addAll(tokenExtractor.extractAllMatches(content, tokenSequence.next()));
        }
        return result;
    }

    public List<NGram> tokenize(Document document) {
        return tokenize(document, NoFilter.getFilter());
    }

    public boolean isIgnoreTitles() {
        return ignoreTitles;
    }

}
