package net.semanticlab.nlp.ngram.helper.sentence;

import com.weblyzard.api.model.Span;
import com.weblyzard.api.model.document.Document;
import com.weblyzard.api.model.document.partition.DocumentPartition;

/**
 * Filter all sentences that are part of a title.
 * 
 * @author Albert Weichselbraun
 *
 */
public class TitleFilter implements SentenceFilter {

    private static final SentenceFilter TITLE_FILTER = new TitleFilter();

    public static SentenceFilter getFilter() {
        return TITLE_FILTER;
    }

    @Override
    public boolean filterSentence(Document document, Span sentence) {
        if (document.getPartitions() == null || !document.getPartitions().containsKey(DocumentPartition.TITLE)) {
            return false;
        }
        return document.getPartitions().get(DocumentPartition.TITLE).stream().anyMatch(span -> overlap(span, sentence));
    }

    protected static boolean overlap(Span s1, Span s2) {
        return s1.getEnd() >= s2.getStart() && s1.getStart() <= s2.getEnd();
    }
}
