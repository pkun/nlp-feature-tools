package net.semanticlab.nlp.dawg;

import java.util.function.Function;
import net.semanticlab.nlp.dawg.node.DawgNodeInterface;

/**
 * Indicates all matches of an input of type V in a {@link DawgNodeInterface} of type T.
 * 
 * @author Albert Weichselbraun
 *
 */
public class DawgAllMatchIndicator<T, V> {

    public enum MatchResult {
        /** a match has been found and further matches are possible. */
        MATCHES,
        /** no match has been found but we need to continue since further matches are possible. */
        CONTINUE,
        /** no further match is possible. */
        BREAK
    }

    private DawgNodeInterface<T> rootNode;
    private final Function<V, T> indexLookupFunction;

    public DawgAllMatchIndicator(DawgNodeInterface<T> rootNode,
                    Function<V, T> indexLookupFunction) {
        this.rootNode = rootNode;
        this.indexLookupFunction = indexLookupFunction;
    }

    public MatchResult matches(V obj) {
        rootNode = rootNode.getChild(indexLookupFunction.apply(obj));
        if (rootNode == null) {
            return MatchResult.BREAK;
        }
        return rootNode.isLeaf() ? MatchResult.MATCHES : MatchResult.CONTINUE;
    }
}
