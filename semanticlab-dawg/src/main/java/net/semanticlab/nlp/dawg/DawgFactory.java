package net.semanticlab.nlp.dawg;

import java.util.Collection;
import java.util.Comparator;
import java.util.List;
import net.semanticlab.nlp.dawg.node.DawgNode;
import net.semanticlab.nlp.dawg.node.DawgNodeInterface;

/**
 * Factory class for creating new DAWGs and determining the maximum rank of the provided needles.
 * 
 * @author Albert Weichselbraun
 *
 */
public class DawgFactory<T> {

    /**
     * Returns a DAWG for the given collection of needles.
     */
    public DawgNodeInterface<T> getDawg(Collection<List<T>> needles) {
        DawgNodeInterface<T> rootNode = new DawgNode<>();
        DawgNodeInterface<T> currentNode;

        for (List<T> needle : needles) {
            currentNode = rootNode;
            for (T letter : needle) {
                currentNode = currentNode.hasChild(letter) ? currentNode.getChild(letter)
                                : currentNode.addChild(letter);
            }
            currentNode.setLeaf(true);
        }
        return rootNode;
    }

    /**
     * Returns the rank (i.e. longest sequence) of the given Collection of needles.
     */
    public int getNeedleRank(Collection<List<T>> needles) {
        return needles.stream().map(List::size).max(Comparator.comparing(Integer::valueOf))
                        .orElse(0);
    }

}
