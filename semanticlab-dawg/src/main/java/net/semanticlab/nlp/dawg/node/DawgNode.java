package net.semanticlab.nlp.dawg.node;

import java.util.HashMap;
import java.util.Map;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * A standard DAWG node.
 * 
 * @author Albert Weichselbraun
 */
@NoArgsConstructor
public class DawgNode<T> implements DawgNodeInterface<T> {

    private Map<T, DawgNodeInterface<T>> children = new HashMap<>();
    @Getter
    @Setter
    private boolean leaf = false;

    @Override
    public DawgNodeInterface<T> getChild(T index) {
        return children.get(index);
    }

    @Override
    public boolean hasChild(T index) {
        return children.containsKey(index);
    }

    /**
     * Adds a new child to the DAWG.
     * 
     * @param child the child to add
     * @param childIndex the child's index
     */
    @Override
    public DawgNodeInterface<T> addChild(T childIndex) {
        return children.computeIfAbsent(childIndex, key -> new DawgNode<>());
    }


}
